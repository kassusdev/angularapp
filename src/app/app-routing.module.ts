import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "../app/components/home/home.component";
import { AppToolbarComponent } from "../app/components/app-toolbar/app-toolbar.component";

/**
 * define all component routes
 */

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "toolbar", component: AppToolbarComponent },
  { path: "", redirectTo: "/home", pathMatch: "full" }, // redirect to `FirstPageComponent`
  // otherwise redirect to PageNotFoundComponent
  { path: "home", component: HomeComponent }, // Wildcard route for a 404 page
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, enableTracing: true }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
